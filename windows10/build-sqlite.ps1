$ErrorActionPreference = "Stop"
. "$PSScriptRoot\ps-support2.ps1"

git clone https://github.com/sqlite/sqlite
cd sqlite
mkdir bld
cd bld

Invoke-CmdScript "C:\BuildTools\VC\Auxiliary\Build\vcvars64.bat"

nmake.exe /f ..\Makefile.msc TOP=..\
nmake.exe /f ..\Makefile.msc sqlite3.c TOP=..\
nmake.exe /f ..\Makefile.msc sqlite3.dll TOP=..\
nmake.exe /f ..\Makefile.msc sqlite3.exe TOP=..\
nmake.exe /f ..\Makefile.msc test TOP=..\

