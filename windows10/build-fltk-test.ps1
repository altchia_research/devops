$ErrorActionPreference = "Stop"
. "$PSScriptRoot\ps-support.ps1"

git clone https://github.com/fltk/fltk.git
cd fltk
mkdir build
cd build
cmake -DCMAKE_LIBRARY_ARCHITECTURE=x64 -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --config Release
cmake --install . --config Release

