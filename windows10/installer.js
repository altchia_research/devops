const { MSICreator } = require('electron-wix-msi');

// Step 1: Instantiate the MSICreator
const msiCreator = new MSICreator({
  appDirectory: 'electron-local-terminal-prototype\\shadowengineterminaltest-win32-x64',
  description: 'My amazing Kitten simulator',
  exe: 'shadowengineterminaltest-win32-x64',
  name: 'Kittens',
  manufacturer: 'Kitten Technologies',
  version: '1.1.2',
  outputDirectory: 'C:\\installer'
});

// Step 2: Create a .wxs template file
//const supportBinaries = await msiCreator.create();

// 🆕 Step 2a: optionally sign support binaries if you
// sign you binaries as part of of your packaging script
//supportBinaries.forEach(async (binary) => {
  // Binaries are the new stub executable and optionally
  // the Squirrel auto updater.
  //await signFile(binary);
//});

msiCreator.create().then(function(){

    msiCreator.compile();
});
