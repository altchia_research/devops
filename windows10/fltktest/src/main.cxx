//
//      Simple Fl_Tabs widget example.
//      Originally from erco's cheat sheet 06/05/2010, permission by author.
//
// Copyright 2010 Greg Ercolano.
// Copyright 1998-2010 by Bill Spitzak and others.
//
// This library is free software. Distribution and use rights are outlined in
// the file "COPYING" which should have been included with this file.  If this
// file is missing or damaged, see the license at:
//
//     https://www.fltk.org/COPYING.php
//
// Please see the following page on how to report bugs and issues:
//
//     https://www.fltk.org/bugs.php
//
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Button.H>
#include <test11.h>

extern Fl_Text_Buffer *sqlite_file_with_settings_txbuf;
long InitializeSettingsFolder();
long CreateSqlTables();

int main(int argc, char *argv[]) {
  Fl::scheme("gtk+");
  Fl_Double_Window *dwin = make_window();
  sqlite_file_with_settings_txbuf = new Fl_Text_Buffer();
  sqlite_file_with_settings_txbuf->text("test/test/test");
  if (InitializeSettingsFolder()) { return 0; }
  if (CreateSqlTables()) { return 0; }
  dwin->show(argc, argv);
  return(Fl::run());
}
