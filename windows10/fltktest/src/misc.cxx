
#include <FL/Fl_Multiline_Input.H>
#include <FL/Fl_Native_File_Chooser.H>
//#include <FL/Fl_Simple_Terminal.H>

#include <stdio.h>
#include <sqlite3.h> 

sqlite3 *settings_db;
  
  // GLOBALS
Fl_Input *G_filename = NULL;
Fl_Multiline_Input *G_filter = NULL;
//Fl_Simple_Terminal *G_tty = NULL;

Fl_Text_Buffer *sqlite_file_with_settings_txbuf = NULL;

char create_settings_sql_tables[] = \
 "CREATE TABLE IF NOT EXISTS CHIA_PLOT_FILES(\n" \
 "  ID            BLOB  PRIMARY KEY NOT NULL,\n" \
 "  K             INT               NOT NULL,\n" \
 "  PATH          TEXT              NOT NULL,\n" \
 "  IS_CHECKED    INT  ,\n" \
 "  SPEED         REAL ,\n" \
 "  IS_REGISTERED INT  ,\n" \
 "  IS_USED       INT   \n" \
 ");\n" \
 "CREATE TABLE IF NOT EXISTS IPFS_PLOT_FILES(\n" \
 "  ID            INT   PRIMARY KEY NOT NULL,\n" \
 "  K             INT               NOT NULL,\n" \
 "  PATH          TEXT              NOT NULL,\n" \
 "  SPEED         REAL  \n" \
 ");\n";

#ifdef __linux__ 

#include <pwd.h>
#include <string>
#include <cstdlib>
#include <sys/stat.h>
#include <unistd.h>

static int sqlite_empty_callback(void *NotUsed, int argc, char **argv, char **azColName) {
	return 0;
}

long CreateSqlTables()
{
	char *zErrMsg = 0;
	if (sqlite3_exec(settings_db, create_settings_sql_tables, sqlite_empty_callback, 0, &zErrMsg) != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		return 1;
	}
	return 0;
}

long InitializeSettingsFolder()
{
	const char* home = getenv("HOME");
	if (home)
	{
		std::string path(home);
		path += "/.plots_and_nodes/settings.db";
		sqlite_file_with_settings_txbuf->text(path.c_str());

		if (chdir(home)) { return 0; }
		if (mkdir(".plots_and_nodes", S_IRWXU)) {
			if (errno != EEXIST) {
				return 0;
			}
		}
		if (chdir(".plots_and_nodes")) { return 0; }

		if (sqlite3_open("settings.db", &settings_db)) { return 0; }

		return 0;
	}
	return 1;
}

#elif _WIN32

//#include "stdafx.h"
#include <windows.h>
#include <KnownFolders.h>
#include <ShlObj.h>
#include <stringapiset.h>
#include <string>

#include <direct.h>
#include <stdlib.h>
#include <stdio.h>

HRESULT InitializeSettingsFolder()
{
	PWSTR path = NULL;
	HRESULT result = SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, NULL, &path);
	if (SUCCEEDED(result)) {

		if (_wchdir(path)) { return 0; }
		if (_mkdir("plots_and_nodes")) {
			if (errno != EEXIST) {
				return 0;
			}
		}
		if (_chdir("plots_and_nodes")) { return 0; }

		int len = WideCharToMultiByte(CP_UTF8, 0, path, -1, 0, 0, 0, 0);
		std::string buf(len, 0);
		WideCharToMultiByte(CP_UTF8, 0, path, -1, &buf[0], len,0,0);
		std::string buf2 = buf.append("\\plots_and_nodes\\settings.db");
		sqlite_file_with_settings_txbuf->text(buf2.c_str());
		CoTaskMemFree(path);

		if (sqlite3_open("settings.db", &settings_db)) { return 0; }

		return 0;
	}
	return 1;
}

#else


#endif
  
void PickChiaPlotFile_CB(Fl_Widget*, void*) {
    printf("%s\n", "kU");
    // Create native chooser
    Fl_Native_File_Chooser native;
    native.title("Pick a Chia plot files");
    native.type(Fl_Native_File_Chooser::BROWSE_MULTI_FILE);
    //native.filter(G_filter->value());
    native.filter("plot-k\?\?-\?\?\?\?-\?\?-\?\?-\?\?-\?\?-\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?.plot");

    //native.preset_file(G_filename->value());
    native.preset_file("");
 
    // Show native chooser
    switch ( native.show() ) {
      case -1: break;      // ERROR
      case  1: fl_beep(); break;           // CANCEL
      default:                                                            // PICKED FILE
    	printf("count %d\n", native.count() );
	for ( int i = 0; i < native.count(); i++ ) {
    		printf("filename %d %s\n", i, native.filename(i));
	}
        /* if ( native.filename() ) {
          //G_filename->value(native.filename());
        } else {
          //G_filename->value("NULL");
        } */
        break;
    }

}
