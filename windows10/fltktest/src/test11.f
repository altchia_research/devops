# data file for the Fltk User Interface Designer (fluid)
version 1.0305
header_name {.h}
code_name {.cxx}
decl {\#include <misc.cxx>} {selected private global
}

Function {make_window()} {open
} {
  Fl_Window {} {
    label {Nodes and Plots} open
    xywh {235 358 340 210} type Double color 206 resizable size_range {340 210 0 0} visible
  } {
    Fl_Menu_Bar {} {open
      xywh {0 0 340 20}
    } {
      Submenu {} {
        label File open
        xywh {5 5 68 24}
      } {
        Submenu {} {
          label {Add plot file} open
          xywh {5 5 68 24}
        } {
          MenuItem {} {
            label {From Chia}
            callback PickChiaPlotFile_CB
            xywh {5 5 34 24}
          }
          MenuItem {} {
            label {For IPFS}
            xywh {5 5 34 24}
          }
          MenuItem {} {
            label {Encrypted plot file}
            xywh {5 5 34 24}
          }
        }
        Submenu {} {
          label Import open
          xywh {5 5 68 24}
        } {
          MenuItem {} {
            label {Encrypted miner keys}
            xywh {5 5 34 24}
          }
        }
        Submenu {} {
          label Export open
          xywh {5 5 68 24}
        } {
          MenuItem {} {
            label {Encrypted miner keys}
            xywh {5 5 34 24}
          }
        }
      }
      Submenu {} {
        label Account open
        xywh {5 5 68 24}
      } {
        MenuItem {} {
          label {Decrypt miner keys to memory}
          xywh {5 5 34 24}
        }
      }
      Submenu {} {
        label Processes open
        xywh {136 -2 68 24}
      } {}
      Submenu {} {
        label Settings open
        xywh {5 5 68 24}
      } {
        MenuItem {} {
          label Paths
          callback {make_settings_paths_window()->show()}
          xywh {0 0 34 24}
        }
      }
      Submenu {} {
        label Help open
        xywh {5 5 68 24}
      } {}
    }
    Fl_Box {} {
      label label
      xywh {0 20 5 170} labeltype NO_LABEL
    }
    Fl_Tabs {} {open
      xywh {5 20 330 170} box OFLAT_BOX resizable
    } {
      Fl_Group {} {
        label {:Status:} open
        xywh {5 40 330 150} hide resizable
      } {
        Fl_Text_Display {} {
          label {Miner ID}
          xywh {60 65 220 20}
        }
        Fl_Text_Display {} {
          label {Status of application}
          xywh {5 115 330 75} align 9
        }
        Fl_Choice {} {
          label {choice:} open
          xywh {5 95 180 20} down_box BORDER_BOX labeltype NO_LABEL
        } {
          MenuItem {} {
            label Quick
            xywh {0 0 34 24}
          }
          Submenu {} {
            label Resources open
            xywh {0 0 68 24}
          } {
            MenuItem {} {
              label {CPU and RAM}
              xywh {0 0 34 24}
            }
            MenuItem {} {
              label {Storage usage}
              xywh {0 0 34 24}
            }
          }
        }
      }
      Fl_Group {} {
        label {:Plots:} open
        xywh {5 40 330 150}
      } {}
      Fl_Group {} {
        label {:Slots:} open
        xywh {30 45 15 15} hide
      } {}
      Fl_Group {} {
        label {:Chains:} open
        xywh {30 45 15 15} hide
      } {}
      Fl_Group {} {
        label {:Statistics:}
        xywh {30 45 15 15} hide
      } {}
      Fl_Group {} {
        label {:Web frontend:} open
        xywh {30 45 15 15} hide
      } {}
    }
    Fl_Box {} {
      label label
      xywh {335 20 5 170} labeltype NO_LABEL
    }
    Fl_Output {} {
      xywh {0 190 340 20} box FLAT_BOX color 206 labeltype NO_LABEL textsize 12 textcolor 24
    }
  }
}

Function {make_settings_paths_window()} {open
} {
  Fl_Window {} {
    label {Nodes and Plots :: Settings :: Paths} open
    xywh {575 286 340 340} type Double color 206 resizable modal size_range {340 340 0 0} visible
  } {
    Fl_Group {} {open
      xywh {0 0 340 340} box DIAMOND_DOWN_BOX color 206
    } {
      Fl_Text_Display sqlite_file {
        label {Sqlite file with settings}
        private xywh {5 25 330 30}
        code0 {sqlite_file->buffer(sqlite_file_with_settings_txbuf);}
      }
    }
  }
}
