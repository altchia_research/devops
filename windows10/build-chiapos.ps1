$ErrorActionPreference = "Stop"
. "$PSScriptRoot\ps-support.ps1"

git clone https://github.com/Chia-Network/chiapos.git
cd chiapos
mkdir build
cd build
cmake ..
cmake --build . --config Release
