$ErrorActionPreference = "Stop"
. "$PSScriptRoot\ps-support.ps1"

git clone https://github.com/substrate-developer-hub/substrate-node-template.git
cd substrate-node-template
cargo build --release
