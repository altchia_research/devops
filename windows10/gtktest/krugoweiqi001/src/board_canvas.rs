use gio::prelude::*;
use gtk::prelude::*;

use gtk::{Application, ApplicationWindow, Button, DrawingArea};

use std::cell::{Cell, RefCell};

use crate::mvar::MVar;

use std::sync::Arc;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum StoneType {
    Black,
    White,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ElementType {
    Stone(StoneType),
    Isolator,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum CanvasMode {
    ViewOnly,
    PlacingElement(ElementType),
}

#[derive(Clone)]
pub struct Canvas {
    /// GTK drawing area widget for the canvas.
    pub widget: Option<gtk::DrawingArea>,

    mvar: MVar<Canvas>,

    // Current mode of the canvas.
    mode: Cell<CanvasMode>,

    /// Popup menu.
    context_menu: RefCell<Option<gtk::Menu>>,
}

impl Canvas {
    pub fn new() -> MVar<Canvas> {
        let mvar = MVar::new_empty();
        mvar.clone().put(Canvas {
            widget: None,
            mvar: mvar.clone(),
            mode: Cell::new(CanvasMode::ViewOnly),
            context_menu: RefCell::new(None),
        });
        mvar
    }

    pub fn init(&mut self) {
        // Create the widget.
        let canvas = gtk::DrawingArea::new();
        canvas.set_size_request(512, 512);
        canvas.set_visible(true);
        canvas.set_sensitive(true);
        canvas.set_can_focus(true);

        // Enable the events you wish to get notified about.
        // The 'draw' event is already enabled by the DrawingArea.
        canvas.add_events(
            gdk::EventMask::BUTTON_PRESS_MASK
                | gdk::EventMask::BUTTON_RELEASE_MASK
                | gdk::EventMask::POINTER_MOTION_MASK
                | gdk::EventMask::SMOOTH_SCROLL_MASK
                | gdk::EventMask::KEY_PRESS_MASK
                | gdk::EventMask::KEY_RELEASE_MASK,
        );

        let mvar = self.mvar.clone();
        canvas.connect_draw(move |widget, context| {
            mvar.lease(|x| x.draw(context));
            Inhibit(true)
        });

        self.widget = Some(canvas);
    }

    fn draw(&self, c: &cairo::Context) {
        if let Some(ref widget) = self.widget {
            self.draw_nested(c, &widget);
        }
    }

    fn draw_nested(&self, ca: &cairo::Context, wi: &gtk::DrawingArea) {
        //let background_color = (0.2f64, 0.2f64, 0.2f64);
        let sc = wi.get_style_context();
        let w = wi.get_allocated_width() as f64;
        let h = wi.get_allocated_height() as f64;
        let draw = Drawing {
            cv: &self,
            ca,
            wi,
            w,
            h,
        };
        //gtk::render_background(&sc, &c, 0.0, 0.0, w, h);
        //c.arc(w/2.0, h/2.0, w.min(h) / 2.0, 0.0, 2.0 * 3.14);
        //c.fill();
        //println!("draw");
        draw.grid();
    }
}

pub struct Drawing<'a> {
    cv: &'a Canvas,
    ca: &'a cairo::Context,
    wi: &'a gtk::DrawingArea,
    w: f64,
    h: f64,
}

impl Drawing<'_> {
    fn grid(&self) {
        let ca = self.ca;
        let a = 1.0 / 18.0;
        let b = 1.0 / 18.0;
        let c = a / 2.0;
        let d = b / 2.0;
        let q = self.w.min(self.h);
        ca.translate(self.w / 2.0, self.h / 2.0);
        ca.scale(q / 1.0, q / 1.0);
        ca.rectangle(-0.6, -0.6, 1.2, 1.2);
        ca.set_source_rgb(0.8, 0.7, 0.4);
        ca.fill();
        let r = 3.14 / 360.0 * (30.0);
        //let r = 0.0;
        ca.rotate(r);
        let sz = 20.0;
        let p = 1.0 / sz;
        let q = p * 2.0;
        for ni in 0..36 {
            for nj in 0..36 {
                let i = (ni as f64 - 18.0) / sz;
                let j = (nj as f64 - 18.0) / sz;

                let u = i * r.cos() - j * r.sin();
                let v = i * r.sin() + j * r.cos();

                if u < -0.5 + p || u > 0.5 - p || v < -0.5 + p || v > 0.5 - p {
                    continue;
                }

                /*
                    if u < -0.6 || u > 0.6 || v < -0.6 || v > 0.6 { continue };
                    ca.arc(i, j, 1.0 / 39.0, 0.0, 2.0 * 3.14);
                    ca.set_source_rgb(0.8, 0.6, 0.4);
                    ca.fill();
                    ca.arc(i, j, 1.0 / 39.0, 0.0, 2.0 * 3.14);
                    ca.set_source_rgb(0.0, 0.0, 0.0);
                    ca.set_line_width(1.0 / self.w);
                    ca.stroke();
                    continue;
                }
                */

                ca.set_line_width(2.0 / self.w);
                ca.set_source_rgb(0.0, 0.0, 0.0);

                if !(u > 0.5 - q || v > 0.5 - p * 1.2) {
                    ca.move_to(i, j);
                    ca.line_to(i + c, j);
                    ca.stroke();
                }

                if !(u < -0.5 + q || v < -0.5 + p * 1.2) {
                    ca.move_to(i, j);
                    ca.line_to(i - c, j);
                    ca.stroke();
                }

                if !(v > 0.5 - q || u < -0.5 + p * 1.2) {
                    ca.move_to(i, j);
                    ca.line_to(i, j + d);
                    ca.stroke();
                }

                if !(v < -0.5 + q || u > 0.5 - p * 1.2) {
                    ca.move_to(i, j);
                    ca.line_to(i, j - d);
                    ca.stroke();
                }
            }
        }
        ca.identity_matrix();
        /*
        ca.translate(self.w / 2.0, self.h / 2.0);
        ca.scale(q / 2.1, q / 2.1);
        ca.set_source_rgb(0.3, 0.1, 0.1);
        ca.set_line_width(10.0 / self.w);

        ca.move_to(-1.0, -1.0);
        ca.line_to(1.0, -1.0);
        ca.stroke();

        ca.move_to(-1.0, 1.0);
        ca.line_to(1.0, 1.0);
        ca.stroke();

        ca.move_to(-1.0, -1.0);
        ca.line_to(-1.0, 1.0);
        ca.stroke();

        ca.move_to(1.0, -1.0);
        ca.line_to(1.0, 1.0);
        ca.stroke();
        */
    }
}
