extern crate cairo;
extern crate gdk;
extern crate gdk_sys;
extern crate gio;
extern crate glib;
extern crate gtk;

pub mod board_canvas;
pub mod mvar;

use mvar::MVar;

use gio::prelude::*;
use gtk::prelude::*;

use gtk::{Application, ApplicationWindow, Button, DrawingArea, MenuBar, Notebook, Label, MenuItem};

fn main() {
    let application =
        Application::new(Some("com.github.gtk-rs.examples.basic"), Default::default())
            .expect("failed to initialize GTK application");

    application.connect_activate(|app| {
        let window = ApplicationWindow::new(app);
        window.set_title("First GTK+ Program");
        window.set_default_size(350, 70);

        let button = Button::new();
        /*
        button.connect_clicked(|_| {
            println!("Clicked!");
        });
        */

        /*
        GtkWidget *drawing_area = gtk_drawing_area_new ();
        gtk_widget_set_size_request (drawing_area, 100, 100);
        g_signal_connect (G_OBJECT (drawing_area), "draw",
                          G_CALLBACK (draw_callback), NULL);
        window.add(drawing_area);
        */

        let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
        let mb = MenuBar::new();
        let nb = Notebook::new();
        let lb01 = Label::new(Some("test"));
        let lb02 = Label::new(Some("test2"));

        let bc = board_canvas::Canvas::new();

        bc.lease_mut(|bc| bc.init());

        bc.lease(|bc| {
            nb.append_page(bc.widget.as_ref().unwrap(), Some(&lb01));
        });
        nb.append_page(&button, Some(&lb02));

        let mi01 = MenuItem::new();
        mi01.set_label("File");
        let mi01a = MenuItem::new();
        mi01a.set_label("test");
        mi01.add(&mi01a);
        mb.append(&mi01);
        vbox.add(&mb);
        vbox.add(&nb);
        window.add(&vbox);

        window.show_all();
    });

    application.run(&[]);
}

/*
gboolean
draw_callback (GtkWidget *widget, cairo_t *cr, gpointer data)
{
  guint width, height;
  GdkRGBA color;
  GtkStyleContext *context;

  context = gtk_widget_get_style_context (widget);

  width = gtk_widget_get_allocated_width (widget);
  height = gtk_widget_get_allocated_height (widget);

  gtk_render_background (context, cr, 0, 0, width, height);

  cairo_arc (cr,
             width / 2.0, height / 2.0,
             MIN (width, height) / 2.0,
             0, 2 * G_PI);

  gtk_style_context_get_color (context,
                               gtk_style_context_get_state (context),
                               &color);
  gdk_cairo_set_source_rgba (cr, &color);

  cairo_fill (cr);

 return FALSE;
}
*/
