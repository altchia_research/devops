use std::sync::Arc;
use std::sync::*;
//use std::sync::MutexGuard;

pub struct MVar<T>(Arc<DirectMVar<T>>);

impl<T> Clone for MVar<T>
where
    Arc<T>: Clone,
{
    fn clone(&self) -> Self {
        MVar(self.0.clone())
    }
}

impl<T> MVar<T> {
    #[inline(always)]
    pub fn new(x: T) -> MVar<T> {
        MVar(Arc::new(DirectMVar::new(x)))
    }

    #[inline(always)]
    pub fn new_empty() -> MVar<T> {
        MVar(Arc::new(DirectMVar::new_empty()))
    }

    #[inline(always)]
    pub fn take(&self) -> T {
        self.0.take()
    }

    #[inline(always)]
    pub fn put(&self, x: T) {
        self.0.put(x)
    }

    #[inline(always)]
    pub fn modify<F: FnOnce(T) -> T>(&self, f: F) {
        self.0.modify(f)
    }

    #[inline(always)]
    pub fn lease<A, F: FnOnce(&T) -> A>(&self, f: F) -> A {
        self.0.lease(f)
    }

    #[inline(always)]
    pub fn lease_mut<A, F: FnOnce(&mut T) -> A>(&self, mut f: F) -> A {
        self.0.lease_mut(f)
    }
}

pub struct DirectMVar<T> {
    lock: Mutex<Option<T>>,
    takers: Condvar,
    putters: Condvar,
}

impl<T> DirectMVar<T> {
    pub fn new(x: T) -> DirectMVar<T> {
        DirectMVar {
            lock: Mutex::new(Some(x)),
            takers: Condvar::new(),
            putters: Condvar::new(),
        }
    }

    pub fn new_empty() -> DirectMVar<T> {
        DirectMVar {
            lock: Mutex::new(None),
            takers: Condvar::new(),
            putters: Condvar::new(),
        }
    }

    pub fn take(&self) -> T {
        let mut guard = self.lock.lock().unwrap();
        loop {
            match guard.take() {
                Some(x) => {
                    self.putters.notify_one();
                    return x;
                }
                None => {}
            }
            guard = self.takers.wait(guard).unwrap();
        }
    }

    pub fn put(&self, x: T) {
        let mut guard = self.lock.lock().unwrap();
        loop {
            match *guard {
                None => {
                    *guard = Some(x);
                    ::std::mem::drop(guard);
                    self.takers.notify_one();
                    return;
                }
                Some(_) => {}
            }
            guard = self.putters.wait(guard).unwrap();
        }
    }

    pub fn modify<F: FnOnce(T) -> T>(&self, f: F) {
        let mut guard = self.lock.lock().unwrap();
        loop {
            match guard.take() {
                Some(x) => {
                    let x_2 = f(x);
                    *guard = Some(x_2);
                    ::std::mem::drop(guard);
                    self.takers.notify_one();
                    return;
                }
                None => {}
            }
            guard = self.takers.wait(guard).unwrap();
        }
    }

    pub fn lease<A, F: FnOnce(&T) -> A>(&self, f: F) -> A {
        let mut guard = self.lock.lock().unwrap();
        loop {
            match &*guard {
                Some(x) => {
                    let y = f(x);
                    ::std::mem::drop(guard);
                    self.takers.notify_one();
                    return y;
                }
                None => {}
            }
            guard = self.takers.wait(guard).unwrap();
        }
    }

    pub fn lease_mut<A, F: FnOnce(&mut T) -> A>(&self, mut f: F) -> A {
        let mut guard = self.lock.lock().unwrap();
        loop {
            match &mut *guard {
                Some(x) => {
                    let y = f(x);
                    ::std::mem::drop(guard);
                    self.takers.notify_one();
                    return y;
                }
                None => {}
            }
            guard = self.takers.wait(guard).unwrap();
        }
    }
}
