$ErrorActionPreference = "Stop"
. "$PSScriptRoot\ps-support.ps1"

cd fltktest
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --config Release

