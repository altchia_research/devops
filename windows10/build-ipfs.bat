


SET PATH=%PATH%;\msys64\usr\bin
pacman --noconfirm -S  git make unzip

git clone https://github.com/ipfs/go-ipfs.git
copy build-ipfs-rules.mk .\go-ipfs\plugin\loader\Rules.mk
cd go-ipfs
make install
