$ErrorActionPreference = "Stop"
. "$PSScriptRoot\ps-support.ps1"

git clone https://github.com/77Z/electron-local-terminal-prototype.git
cd electron-local-terminal-prototype
yarn install
yarn rebuild
electron-packager .
