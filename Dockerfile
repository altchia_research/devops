FROM alpine/git:v2.30.1@sha256:426e129b37e0bfbe18acff5ad895e4165d9aa2e07441e07b59b809c95815270a as git

FROM nixos/nix:2.3.10@sha256:d7a6fac6542d60d8991362bed26cc1e50ad2d1a61db9d4dcde946cf8d0efcdf3 as nix

ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt

ARG INTERP_HASH="93057d90fc853f5c74bd2d857620ca4734c025f8eadbe7eb6c383e6bc83da2cd"
COPY ./monkey/interp.sh /
RUN echo ${INTERP_HASH} ' /interp.sh' | sha256sum -c /dev/stdin
SHELL ["/interp.sh"]

RUN nix-env -i git

FROM nix as nixpkgs

ARG MOZILLA_OVERLAY_REF="57c8084c7ef41366993909c20491e359bbb90f54"
ARG NIXPKGS_TAG="20.09"
ARG NIXPKGS_SHA256="1wg61h4gndm3vcprdcg7rc4s1v3jkm5xd7lw8r2f67w502y94gcy"
RUN cat > /nixpkgs.nix \
@@@ let \
@@@   mozillaOverlay = \
@@@     import (builtins.fetchGit { \
@@@       url = "https://github.com/mozilla/nixpkgs-mozilla.git"; \
@@@       rev = "${MOZILLA_OVERLAY_REF}"; \
@@@     }); \
@@@   nixpkgs = \
@@@     import (fetchTarball { \
@@@       url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/${NIXPKGS_TAG}.tar.gz"; \
@@@       sha256 = "${NIXPKGS_SHA256}"; \
@@@     }) { overlays = [ mozillaOverlay ]; }; \
@@@ in \
@@@ nixpkgs

RUN nix-env-install \
@@@ with (import /nixpkgs.nix ); \
@@@ buildEnv { \
@@@   name = "custom-nixpkgs-env"; \
@@@   paths = []; \
@@@ }

FROM nixpkgs as rust

ARG RUST_DATE="2021-03-01"
RUN cat > /rust-nightly.nix \
@@@ let \
@@@   nixpkgs = import /nixpkgs.nix; \
@@@   rust-nightly = with nixpkgs; ((rustChannelOf { date = "${RUST_DATE}"; channel = "nightly"; }).rust.override { \
@@@     targets = [ "wasm32-unknown-unknown" ]; \
@@@     extensions = ["rust-src"]; \
@@@   }); \
@@@ in \
@@@ rust-nightly

RUN nix-env-install \
@@@ let \
@@@   nixpkgs = import /nixpkgs.nix; \
@@@   rust-nightly = import /rust-nightly.nix; \
@@@ in \
@@@ with nixpkgs; \
@@@ buildEnv { \
@@@   name = "rust-for-substrate-env"; \
@@@   paths = [ clang cmake pkg-config rust-nightly ]; \
@@@ }

ARG ADDITIONAL_BUILD_TOOLS="rake"
RUN nix-env-install \
@@@ with (import /nixpkgs.nix ); \
@@@ buildEnv { \
@@@   name = "buildtools-env"; \
@@@   paths = [ cached-nix-shell gnumake jq ${ADDITIONAL_BUILD_TOOLS} ]; \
@@@ }

RUN cat > /shell-for-substrate.nix \
@@@ with (import /nixpkgs.nix ); pkgs.mkShell { \
@@@   buildInputs = [ (import /rust-for-substrate-env.nix) ]; \
@@@   LIBCLANG_PATH = "${llvmPackages.libclang}/lib"; \
@@@   PROTOC = "${protobuf}/bin/protoc"; \
@@@   ROCKSDB_LIB_DIR = "${rocksdb}/lib"; \
@@@ }

RUN sh \
@@@ cached-nix-shell /shell-for-substrate.nix --run 'rustc --version && echo $PROTOC | grep protobuf' \
@@@ mkdir /target

ENV CARGO_TARGET_DIR="/target"

FROM rust as build-tools

ARG RUNTIME_HASHER_VERSION="v0.2.9"
ARG RUNTIME_HASHER_NAME="substrate-runtime-hasher-${RUNTIME_HASHER_VERSION}"
ARG RUNTIME_HASHER_FILENAME="${RUNTIME_HASHER_NAME}.tar.gz"
ARG RUNTIME_HASHER_URL="https://gitlab.com/chevdor/substrate-runtime-hasher/-/archive/${RUNTIME_HASHER_VERSION}/${RUNTIME_HASHER_FILENAME}"
ARG RUNTIME_HASHER_SHA256="8b85d0cb86a6b4a1f9e9bedea12030a5bac07204b8fd090f7a0758d561fa1c4a"
RUN sh \
@@@ wget ${RUNTIME_HASHER_URL} \
@@@ echo ${RUNTIME_HASHER_SHA256} ' ${RUNTIME_HASHER_FILENAME}' | sha256sum -c /dev/stdin \
@@@ tar -xzvf ${RUNTIME_HASHER_FILENAME} \
@@@ cd ${RUNTIME_HASHER_NAME} \
@@@ cached-nix-shell /shell-for-substrate.nix --run 'cargo build --release' \
@@@ cd .. \
@@@ rm -Rf ${RUNTIME_HASHER_NAME}*

RUN exit 1

# This is commit from rococo-v1 branch.
#ARG POLKADOT_COMMIT="8eb16510f9fbdd077d0ac3edf304591b8f689eab"
#ARG POLKADOT_FILENAME="${POLKADOT_COMMIT}.tar.gz"
#ARG POLKADOT_URL="https://github.com/paritytech/polkadot/archive/${POLKADOT_FILENAME}"
#ARG POLKADOT_SHA256="9f9c655532ccdbf82f70018c369c7ffa32f90f61c5e69fd5ecfd8db6de0fb77d"
#ARG POLKADOT_DIR="polkadot-${POLKADOT_COMMIT}"
#RUN sh \
#@@@ wget ${POLKADOT_URL} \
#@@@ echo ${POLKADOT_SHA256} ' ${POLKADOT_FILENAME}' | sha256sum -c /dev/stdin \
#@@@ tar -xzvf ${POLKADOT_FILENAME} \
#@@@ cd ${POLKADOT_DIR} \
#@@@ cached-nix-shell /shell-for-substrate.nix --run 'cargo build --release' \
#@@@ cd .. \
#@@@ rm -Rf *${POLKADOT_COMMIT}*


FROM build-tools as prebuild-substrate-node

COPY .dockerignore .git-for-prebuild.tar* /tmp/

RUN ls -la /tmp/

RUN exit 1

RUN sh \
@@@ mkdir /tmp/source \
@@@ tar -xf /tmp/source.tar -C /tmp/source \
@@@ git clone /tmp/source/.git repo \
@@@ git clone /tmp/source/.git/modules/share repo/share \
@@@ git clone /tmp/source/.git/modules/substrate repo/substrate \
@@@ rm -Rf /tmp/source*

RUN exit 1

#FIXME: escape is fixed in shared repo.
#USE PATTERN
RUN sh \
@@@ find repo -type f -not -regex '.*/\\.git/.*' -exec sha256sum {} \\; | sort > repo.sha256 \
@@@ sha256sum -c repo.sha256 \
@@@ sha256sum repo.sha256 > repo.total.sha256

WORKDIR /repo

RUN cached-nix-shell /shell-for-substrate.nix --run 'cargo build --release'

FROM prebuild-substrate-node as build-substrate-node

#FIXME
RUN mkdir -p /tmp/source

COPY .git /tmp/source/.git

#TODO: fix bug with comment

ARG BUILD_TRIGGER_REGEX="(\.rs)$"
ARG GIT_COMMIT_TO_BUILD=""
RUN sh \
@@@ commit_prebuild=`git rev-parse --verify HEAD` \
@@@ if [ "${GIT_COMMIT_TO_BUILD}" == "" ]; then \
@@@   commit_to_build=`cd /tmp/source && git rev-parse --verify HEAD` \
@@@ else \
@@@   commit_to_build=${GIT_COMMIT_TO_BUILD} \
@@@ fi \
@@@ test "$commit_prebuild" == "$commit_to_build" && exit 0 \
@@@ git pull --all \
@@@ git checkout $commit_to_build \
@@@ git diff --name-only $commit_prebuild $commit_to_build > /tmp/diff_names.txt \
@@@ cat /tmp/diff_names.txt \
@@@ if grep -q -E '${BUILD_TRIGGER_REGEX}' /tmp/diff_names.txt; then \
@@@   cached-nix-shell /shell-for-substrate.nix --run 'cargo build --release' \
@@@ else \
@@@   echo Build is not needed, using prebuilded files \
@@@ fi

#FIXME: escape is fixed in shared repo.
RUN sh \
@@@ mkdir /export \
@@@ find /repo -type f -not -regex '.*/\\.git/.*' -exec sha256sum {} \\; | sort > /repo.sha256 \
@@@ sha256sum /repo.sha256 > /repo.total.sha256 \
@@@ cp /repo.total.sha256 /export/

COPY ./share/collect_paths.sh /collect_paths.sh

ARG BINARIES_TO_EXPORT="/target/release/node-template /target/release/substrate-runtime-hasher"
RUN sh \
@@@ /collect_paths.sh ${BINARIES_TO_EXPORT} | sort | uniq > deps_paths.txt \
@@@ cat deps_paths.txt \
@@@ tar -h -cf deps.tar \`cat deps_paths.txt\` \
@@@ mkdir /tmp/deps \
@@@ tar -xvf deps.tar -C /tmp/deps \
@@@ rm -Rvf deps*

FROM alpine as basic

COPY --from=build-substrate-node /tmp/deps /
COPY --from=build-substrate-node /export /export

